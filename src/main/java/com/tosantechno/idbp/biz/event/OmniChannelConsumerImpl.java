package com.tosantechno.idbp.biz.event;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tosantechno.idbp.biz.config.KafkaProperties;
import com.tosantechno.idbp.biz.service.IGeneralService;
import com.tosantechno.idbp.biz.service.IGetSequenceService;
import com.tosantechno.idbp.biz.service.MultiBankSupportStrategy;
import com.tosantechno.idbp.common.event.annotation.TosanConsumer;
import com.tosantechno.idbp.model.OmniChannel;
import com.tosantechno.idbp.model.event.ProcessorConsumer;
import com.tosantechno.idbp.model.message.InvocationMessage;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by user on 9/12/2020.
 */
@EnableKafka
@Component
@TosanConsumer
public class OmniChannelConsumerImpl implements ProcessorConsumer
{
    @Autowired
    private MultiBankSupportStrategy multiBankSupportStrategy;

    @Autowired
    private KafkaProperties configProperties;

    @Autowired
    private IGeneralService iGeneralService;

    @Autowired
    private IGetSequenceService iGetSequenceS;

    @Override
    @KafkaListener(
            topics = "#{'${kafka-config.OmniChannelListenerTopic}'.split(',')}",
            groupId = "#{'${kafka-config.OmniChannelListenerGroup}'}",
            containerFactory = "concurrentKafkaListenerContainerFactory")
    @SendTo
    public String getMessage(ConsumerRecord<String, String> record,
                             @Header(KafkaHeaders.REPLY_TOPIC) String replyTopicName) throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        InvocationMessage message = mapper.readValue(record.value(), new TypeReference<InvocationMessage>() {
        });

        try {
            switch (message.getHeader().getProcessType()) {
                case PT_INSERT_EMPLOYEE:
                   // message = employeeService.save(message);
                    multiBankSupportStrategy.runStartOmniChannel(message);
                    return new ObjectMapper().writeValueAsString(message);
                case PT_EDIT_EMPLOYEE:
                    multiBankSupportStrategy.runProcessOmniChannel(message);
                    return new ObjectMapper().writeValueAsString(message);
                case PT_FETCH_ALL_EMPLOYEE:
                    multiBankSupportStrategy.runEndOmniChannel(message);
                    return new ObjectMapper().writeValueAsString(message);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            message.getResultMessage().setErrorCode(100);
        }

        return new ObjectMapper().writeValueAsString(message);
    }


}

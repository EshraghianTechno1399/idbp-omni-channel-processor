package com.tosantechno.idbp.biz.service;

import com.tosantechno.idbp.biz.dao.IOmniChannelRepository;
import com.tosantechno.idbp.biz.dao.OmniChannelRepositoryImpl;
import com.tosantechno.idbp.model.OmniChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IranZaminOmniChannelServiceImpl  implements IOmniChannelService
{
    @Autowired
    private OmniChannelRepositoryImpl omniChannelRepository;

    @Autowired
    private IOmniChannelRepository iOmniChannelRepository;

    @Autowired
    private GetSequenceServiceImpl getSequenceService;


    @Override
    public void startOmniChannel(OmniChannel omniChannel)
    {
        omniChannelRepository.saveOmniChannel(omniChannel);
    }

    @Override
    public void processOmniChannel(OmniChannel omniChannel)
    {
        omniChannelRepository.saveOmniChannel(omniChannel);
    }

    @Override
    public void endOmniChannel(OmniChannel omniChannel)
    {
        omniChannelRepository.saveOmniChannel(omniChannel);
    }

}

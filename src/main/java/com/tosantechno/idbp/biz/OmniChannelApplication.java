package com.tosantechno.idbp.biz;


import com.tosantechno.idbp.biz.config.KafkaProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EnableConfigurationProperties({KafkaProperties.class })
public class OmniChannelApplication
{
    public static void main(String[] args)
    {

        SpringApplication.run(OmniChannelApplication.class);
    }
}

package com.tosantechno.idbp.biz.service;


import com.tosantechno.idbp.model.OmniChannel;

public interface IOmniChannelService
{

    void startOmniChannel(OmniChannel omniChannel);

    void processOmniChannel(OmniChannel omniChannel);

    void endOmniChannel(OmniChannel omniChannel);


}

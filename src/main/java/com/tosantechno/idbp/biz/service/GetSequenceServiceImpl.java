package com.tosantechno.idbp.biz.service;

import com.tosantechno.idbp.biz.dao.ISequenceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class GetSequenceServiceImpl implements IGetSequenceService
{
    @Autowired
    private ISequenceDao iSequenceDao;

    @Override
    public long getNextSequenceId(String key) {
        return iSequenceDao.getNextSequenceId(key);
    }
}

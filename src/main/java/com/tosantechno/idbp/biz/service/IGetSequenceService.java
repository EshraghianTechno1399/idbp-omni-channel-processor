package com.tosantechno.idbp.biz.service;

public interface IGetSequenceService
{
    long getNextSequenceId(String key);
}

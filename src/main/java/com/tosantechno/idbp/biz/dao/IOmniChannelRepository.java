package com.tosantechno.idbp.biz.dao;

import ch.qos.logback.core.boolex.EvaluationException;
import com.sun.org.apache.xpath.internal.objects.XBoolean;
import com.tosantechno.idbp.model.OmniChannel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IOmniChannelRepository  extends MongoRepository <OmniChannel, String>
{

    List<OmniChannel>  findByNationalCode(String nationalCode);

    public List<OmniChannel> findByCode(String code);

    OmniChannel findByCodeAndStep(String code , Integer step);
}

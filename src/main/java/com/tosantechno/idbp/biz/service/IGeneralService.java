package com.tosantechno.idbp.biz.service;


import com.tosantechno.idbp.model.AggregateOmniChannel;
import com.tosantechno.idbp.model.OmniChannel;

import java.util.List;

public interface IGeneralService
{
    List<OmniChannel> getOmniChannelByNationalCode(String nationalCode);

    List<AggregateOmniChannel> getAggregateInformation(String code);

    Boolean codeIsValid(String code);

    OmniChannel getOmniChannelByCode(String code);

}

package com.tosantechno.idbp.biz.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvalidCodeException extends RuntimeException
{
    private String errCode;
    private String errMsg;


    public InvalidCodeException(String errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public InvalidCodeException(String errMsg) {
        this.errMsg = errMsg;
    }
}

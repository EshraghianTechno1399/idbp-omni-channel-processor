package com.tosantechno.idbp.biz.dao;

import org.springframework.transaction.annotation.Transactional;


public interface ISequenceDao
{
        long getNextSequenceId(String key);
}

package com.tosantechno.idbp.biz.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("kafka-config")
@Getter
@Setter
public class KafkaProperties
{
    private String requestTopicOmniChannel;
    private String requestReplyOmniChannel;
    private String bootstrapServers;
    private String groupIdOmniChannelEndpoint;
    private String keySequence;
    private String bankId;

}

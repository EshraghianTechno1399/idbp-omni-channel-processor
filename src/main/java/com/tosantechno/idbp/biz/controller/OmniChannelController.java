package com.tosantechno.idbp.biz.controller;


import com.tosantechno.idbp.biz.config.KafkaProperties;
import com.tosantechno.idbp.biz.exception.InvalidCodeException;

import com.tosantechno.idbp.biz.service.IGeneralService;
import com.tosantechno.idbp.biz.service.IGetSequenceService;
import com.tosantechno.idbp.biz.service.MultiBankSupportStrategy;
import com.tosantechno.idbp.model.AggregateOmniChannel;
import com.tosantechno.idbp.model.OmniChannel;
import com.tosantechno.idbp.model.OmniChannelStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
public class OmniChannelController
{

    @Autowired
    private MultiBankSupportStrategy multiBankSupportStrategy;

    @Autowired
    private KafkaProperties configProperties;

    @Autowired
    private IGeneralService iGeneralService;

    @Autowired
    private IGetSequenceService iGetSequenceService;


//    @PostMapping("/startOmniChannel")
//    public String startOmniChannel(@RequestBody OmniChannel omniChannelModel)
//    {
//        OmniChannel omniChannel = fillOmniChannelObject(omniChannelModel, false);
//        multiBankSupportStrategy.StartOmniChannel(omniChannel);
//        return "true";
//    }

//    @PostMapping("/processOmniChannel")
//    public String processOmniChannel(@RequestBody OmniChannelStep inputOmniChannelStep)
//    {
//        try
//        {
//            OmniChannel omniChannelStep = fillOmniChannelStepObject(inputOmniChannelStep);
//            if (iGeneralService.codeIsValid(omniChannelStep.getCode())) {
//                multiBankSupportStrategy.processOmniChannel(omniChannelStep);
//                return "true";
//            }
//        }
//        catch (InvalidCodeException ex)
//        {
//            return "false";
//        }
//        return "false";
//    }

//    @PostMapping("/endOmniChannel/{code}")
//    public Boolean endOmniChannel(@PathVariable String code)
//    {
//        OmniChannel omniChannel = iGeneralService.getOmniChannelByCode(code);
//
//        OmniChannel om = fillOmniChannelObject(omniChannel, true);
//        multiBankSupportStrategy.endOmniChannel(om);
//        return true;
//    }

    @GetMapping("/getOmniChannel/{nationalCode}")
    public List<OmniChannel> getOmniChannel(@PathVariable String nationalCode)
    {
     //   return  multiBankSupportStrategy.runGetCustomerByNationalCode(nationalCode);
        return iGeneralService.getOmniChannelByNationalCode(nationalCode);
    }

    @GetMapping("/getAggregateInformation/{code}")
    public List<AggregateOmniChannel> getAggregateInformation(@PathVariable String code)
    {
        //return multiBankSupportStrategy.runGetAggregateInformation();
        return iGeneralService.getAggregateInformation(code);
    }




//    private OmniChannel fillOmniChannelObject(OmniChannel omniChannelModel , boolean end)
//{
//
//    OmniChannel omniChannel= new OmniChannel();
//    omniChannel.setStartChannel(omniChannelModel.getStartChannel());
//    omniChannel.setServiceType(omniChannelModel.getServiceType());
//    omniChannel.setBankId(omniChannelModel.getBankId());
//    if (!end)
//
//    {
//        omniChannel.setCode(String.valueOf(iGetSequenceService.getNextSequenceId(configProperties.getKeySequence())));
//        omniChannel.setStatus(false);
//    }else
//    {
//        omniChannel.setCode(omniChannelModel.getCode());
//        omniChannel.setStatus(true);
//    }
//    omniChannel.setExternalCode(omniChannelModel.getExternalCode());
//    omniChannel.setNationalCode(omniChannelModel.getNationalCode());
//    omniChannel.setCreateDate(new Date());
//    omniChannel.setStep(omniChannelModel.getStep());
//    omniChannel.setData(omniChannelModel.getData());
//    return omniChannel;
//}

//    private OmniChannel fillOmniChannelStepObject(OmniChannelStep inputOmniChannelStep)
//    {
//        OmniChannel omniChannelStep=new OmniChannel();
//        omniChannelStep.setCode(inputOmniChannelStep.getCode());
//        omniChannelStep.setStep(inputOmniChannelStep.getStep());
//        omniChannelStep.setData(inputOmniChannelStep.getData());
//        return omniChannelStep;
//
//    }

}

package com.tosantechno.idbp.biz.service;

import com.tosantechno.idbp.biz.dao.IOmniChannelRepository;
import com.tosantechno.idbp.biz.dao.OmniChannelRepositoryImpl;
import com.tosantechno.idbp.model.AggregateOmniChannel;
import com.tosantechno.idbp.model.OmniChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GeneralServiceImpl  implements  IGeneralService
{
    @Autowired
    private IOmniChannelRepository iOmniChannelRepository;

    @Autowired
    private OmniChannelRepositoryImpl omniChannelRepository;


    public List<OmniChannel> getOmniChannelByNationalCode(String nationalCode)
    {
        return  iOmniChannelRepository.findByNationalCode(nationalCode);
    }

    public List<AggregateOmniChannel> getAggregateInformation()
    {
        return omniChannelRepository.getAggregateInformation();
    }

    @Override
    public List<AggregateOmniChannel> getAggregateInformation(String code) {
        return omniChannelRepository.getAggregateInformation(code);
    }

    @Override
    public Boolean codeIsValid(String code) {
        List<OmniChannel> omniChannelList =iOmniChannelRepository.findByCode(code);
        if (omniChannelList.size()>0)
            return true;
        return false;
    }

    @Override
    public OmniChannel getOmniChannelByCode(String code) {
        return iOmniChannelRepository.findByCodeAndStep(code , 0);
    }
}

package com.tosantechno.idbp.biz.dao;

import com.tosantechno.idbp.model.AggregateOmniChannel;
import com.tosantechno.idbp.model.OmniChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Repository
public class OmniChannelRepositoryImpl
{
    @Autowired
    private IOmniChannelRepository iOmniChannelRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public void saveOmniChannel(OmniChannel omniChannel)
    {
        try
        {
            iOmniChannelRepository.save(omniChannel);
        }
        catch (Exception e)
        {
            System.out.println("Error in Saving OmniChannel");
            e.printStackTrace();
        }

    }
    public void saveOmniChannelStep(OmniChannel omniChannelStep)
    {
        try {
            iOmniChannelRepository.save(omniChannelStep);
        }
        catch (Exception e)
        {
            System.out.println("Error in Saving OmniChannelStep");
            e.printStackTrace();
        }

    }
    public List<AggregateOmniChannel> getAggregateInformation()
    {
        GroupOperation groupByCodeAndServiceType = group("code")
                .max("step").as("max_step");

      //  MatchOperation filterOmnichannel = match(new Criteria("maxStep").lt(50000));
      //  SortOperation sortByPopDesc = sort(new Sort(Sort.Direction.DESC, "statePop"));

        Aggregation aggregation = newAggregation(groupByCodeAndServiceType);
        AggregationResults<AggregateOmniChannel> result = mongoTemplate.aggregate(
                aggregation, "TotalOmniChannel", AggregateOmniChannel.class);

     //   AggregateOmniChannel smallestState = result.getUniqueMappedResult();
        List<AggregateOmniChannel> aggregateResult = result.getMappedResults();
        return aggregateResult;
    }
    public List<AggregateOmniChannel> getAggregateInformation(String code)
    {
        GroupOperation groupByCode = group("code").max("step").as("max_step");

        MatchOperation filterOmnichannel = match(new Criteria("code").is(code));

        Aggregation aggregation = newAggregation(filterOmnichannel,groupByCode);
        AggregationResults<AggregateOmniChannel> result = mongoTemplate.aggregate(
                aggregation, "TotalOmniChannel", AggregateOmniChannel.class);

        List<AggregateOmniChannel> aggregateResult = result.getMappedResults();
        return aggregateResult;
    }


}

package com.tosantechno.idbp.biz.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tosantechno.idbp.biz.config.KafkaProperties;
import com.tosantechno.idbp.biz.exception.InvalidCodeException;
import com.tosantechno.idbp.model.OmniChannel;
import com.tosantechno.idbp.model.OmniChannelStep;
import com.tosantechno.idbp.model.message.InvocationMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.REPEATABLE_READ)
public class MultiBankSupportStrategy
{
    @Autowired
    public KafkaProperties kafkaProperties;

    @Autowired
    private KafkaProperties configProperties;

    @Autowired
    private IOmniChannelService iOmniChannelService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private IGetSequenceService iGetSequenceService;


    @Autowired
    private IGeneralService iGeneralService;

    private String findBankType()
    {
        String bankId = kafkaProperties.getBankId();
        switch (bankId)
        {
            case "0":
                iOmniChannelService = (IOmniChannelService) applicationContext.getBean("iranZaminOmniChannelServiceImpl");
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + bankId);
        }
        return bankId;
    }


    public void runStartOmniChannel(InvocationMessage message) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        OmniChannel omniChannel = mapper.readValue(mapper.writeValueAsString(message.getPayload().get("OmniChannel")), OmniChannel.class);

        OmniChannel newOmniChannel = fillOmniChannelObject(omniChannel, false);
        findBankType();
        iOmniChannelService.startOmniChannel(newOmniChannel);
    }

    public void runProcessOmniChannel(InvocationMessage message) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        OmniChannelStep omniChannelStep = mapper.readValue(mapper.writeValueAsString(message.getPayload().get("OmniChannelStep")), OmniChannelStep.class);

        try
        {
            OmniChannel omniChannel = fillOmniChannelStepObject(omniChannelStep);
            if (iGeneralService.codeIsValid(omniChannelStep.getCode())) {
                findBankType();
                iOmniChannelService.processOmniChannel(omniChannel);
            }
        }
        catch (InvalidCodeException ex)
        {
            System.out.println("dhdsfh");
        }

    }

    public void runEndOmniChannel(InvocationMessage message) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String code = mapper.readValue(mapper.writeValueAsString(message.getPayload().get("code")), String.class);

        OmniChannel omniChannel = iGeneralService.getOmniChannelByCode(code);

        OmniChannel om = fillOmniChannelObject(omniChannel, true);
        iOmniChannelService.endOmniChannel(om);

    }

//    public void StartOmniChannel(OmniChannel omniChannel)
//    {
//        findBankType();
//        iOmniChannelService.startOmniChannel(omniChannel);
//    }

//    public void processOmniChannel(OmniChannel omniChannel)
//    {
//        findBankType();
//        iOmniChannelService.processOmniChannel(omniChannel);
//    }

//    public void endOmniChannel(OmniChannel omniChannel)
//    {
//        findBankType();
//        iOmniChannelService.endOmniChannel(omniChannel);
//    }

    private OmniChannel fillOmniChannelObject(OmniChannel omniChannelModel , boolean end)
    {

        OmniChannel omniChannel= new OmniChannel();
        omniChannel.setStartChannel(omniChannelModel.getStartChannel());
        omniChannel.setServiceType(omniChannelModel.getServiceType());
        omniChannel.setBankId(omniChannelModel.getBankId());
        if (!end)

        {
            omniChannel.setCode(String.valueOf(iGetSequenceService.getNextSequenceId(configProperties.getKeySequence())));
            omniChannel.setStatus(false);
        }else
        {
            omniChannel.setCode(omniChannelModel.getCode());
            omniChannel.setStatus(true);
        }
        omniChannel.setExternalCode(omniChannelModel.getExternalCode());
        omniChannel.setNationalCode(omniChannelModel.getNationalCode());
        omniChannel.setCreateDate(new Date());
        omniChannel.setStep(omniChannelModel.getStep());
        omniChannel.setData(omniChannelModel.getData());
        return omniChannel;
    }

    private OmniChannel fillOmniChannelStepObject(OmniChannelStep inputOmniChannelStep)
    {
        OmniChannel omniChannelStep=new OmniChannel();
        omniChannelStep.setCode(inputOmniChannelStep.getCode());
        omniChannelStep.setStep(inputOmniChannelStep.getStep());
        omniChannelStep.setData(inputOmniChannelStep.getData());
        return omniChannelStep;

    }


}
